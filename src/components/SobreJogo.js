import React, { Component } from 'react';
import { Text, StyleSheet, View } from 'react-native';

export default class SobreJogo extends Component{
    render(){
        return(
            <View style={styles.viewSobre}>
                <Text style={styles.txtSobre}>
                    Jogo de cara ou coroa feito em React Native versão 0.44. Para Android. Aprendizado do componente React-Native-Router-Flux, que optimiza o desenvolvimento de cenas(telas de transição) no aplicativo.
                </Text>
                <Text style={styles.txtSobre}> Contato: michelvictor16@gmail.com</Text>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    viewSobre:{
        flex:1,
        backgroundColor: '#61BD8C'
    },
    txtSobre:{
        color: '#FFF',
        fontSize: 20,
        margin: 20,
        textAlign: 'justify'
    }
})